import cv2
import numpy as np
import random
import time

start_point = [0, 10]
start_shape_index = 0
vertex_circle_thickness = 5
trace_circle_thickness = 1
# proportion = 0.45
proportion = 0.5
scale = 0.55
shapes_data = \
    [
        [  # shape 1
            [  # vertex data for shape 1
                [  # vertex 1
                    [int(500 * scale), int(0 * scale)], 1/6  # coordinate, probability of landing there within the shape
                ],
                [
                    [int(1500 * scale), int(0 * scale)], 1/6
                ],
                [
                    [int(2000 * scale), int(866 * scale)], 1/6
                ],
                [
                    [int(1500 * scale), int(1732 * scale)], 1/6
                ],
                [
                    [int(500 * scale), int(1732 * scale)], 1/6
                ],
                [
                    [int(0 * scale), int(860 * scale)], 1/6
                ]
            ],
            1/5  # probability of going to another shape
        ]
    ]


def are_shapes_valid(shapes_data):
    if start_shape_index < 0 or start_shape_index >= len(shapes_data):
        print("Invalid start shape index.")
        return False
    for shape_data in shapes_data:
        vertex_probability_sum = 0.0
        for vertex_data in shape_data[0]:
            vertex_probability_sum += vertex_data[1]
            if len(vertex_data[0]) != 2:
                print("Not a proper vertex.")
                return False
        if not 0.99999 <= vertex_probability_sum <= 1:
            print("Invalid probability sum: ", vertex_probability_sum)
            return False
    return True


def get_plot_dimensions(current_image, shapes_data, start_point):

    max_width = max(abs(start_point[0]), current_image.shape[1])
    max_height = max(abs(start_point[1]), current_image.shape[0])
    for shape_data in shapes_data:
        for vertex_data in shape_data[0]:
            max_width = max(abs(vertex_data[0][0]), max_width)
            max_height = max(abs(vertex_data[0][1]), max_height)
    return tuple((int(max_height) + 10, int(max_width) + 10, 3))


if __name__ == "__main__":
    assert are_shapes_valid(shapes_data)
    img = np.zeros(get_plot_dimensions(np.zeros((512, 512, 3), np.uint8), shapes_data, start_point), np.uint8)
    for shape_data in shapes_data:
        for vertex_data in shape_data[0]:
            cv2.circle(img, tuple(vertex_data[0]), vertex_circle_thickness, (255, 0, 0))

    cv2.circle(img, tuple(start_point), trace_circle_thickness, (0, 255, 0), -1)
    cv2.imshow("Fractal plot", img)
    pressed_key = cv2.waitKey()

    current_shape_index = int(start_shape_index)
    while pressed_key != 27:  # 27 is the code for the Esc key on the keyboard
        random_vertex_probability = random.uniform(0, 1)
        selected_vertex_index = 0
        while random_vertex_probability > 0:
            random_vertex_probability -= shapes_data[current_shape_index][0][selected_vertex_index][1]
            if random_vertex_probability <= 0:
                break
            selected_vertex_index += 1
        destination_point = [
            int(start_point[0] + proportion * (
                    shapes_data[current_shape_index][0][selected_vertex_index][0][0] - start_point[0])),
            int(start_point[1] + proportion * (
                    shapes_data[current_shape_index][0][selected_vertex_index][0][1] - start_point[1]))
        ]
        # destination_point = [
        #     int((start_point[0] + shapes_data[current_shape_index][0][selected_vertex_index][0][0]) * proportion),
        #     int((start_point[1] + shapes_data[current_shape_index][0][selected_vertex_index][0][1]) * proportion)
        # ]

        # print("Status report:")
        # print("Current shape index: ", current_shape_index)
        # print("Selected vertex index: ", selected_vertex_index)
        # print("Selected vertex: ", shapes_data[current_shape_index][0][selected_vertex_index][0])
        # print("Start point: ", start_point)
        # print("Destination point: ", destination_point)

        cv2.rectangle(img, tuple(destination_point), tuple(destination_point), (0, 255, 0))
        start_point = destination_point
        cv2.imshow("Fractal plot", img)
        pressed_key = cv2.waitKey(1)

    cv2.imwrite("gen/fractal-" + time.strftime('%b-%d-%Y_%H%M', time.localtime()) + ".png", img)
